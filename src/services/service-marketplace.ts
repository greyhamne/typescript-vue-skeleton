import { oioiApiInstance } from '@/services/service-instances.api'

const getListing = (id: any) => {
    try {
        return oioiApiInstance
          .get('/api/marketplace/listing/' + id);
    }
    catch (e) {
        return e;
    }
};

const getListings = (userId: string) => {
    try {
      return oioiApiInstance
        .get(`/api/marketplace/${userId}`);
    }
    catch (e) {
      return e;
    }
  }


export default {
    getListing,
    getListings
};
