import { oioiApiInstance } from '@/services/service-instances.api'

///////////////////////////////////////////////////////////////////////////
// GET my listings
///////////////////////////////////////////////////////////////////////////

const getMyListings = (id: any) => {
    try {
        return oioiApiInstance
          .get('/api/listings/' + id);
    }
    catch (e) {
        return e;
    }
};



export default {
    getMyListings,
};
