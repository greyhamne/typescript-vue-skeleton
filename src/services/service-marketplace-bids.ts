import { oioiApiInstance } from '@/services/service-instances.api'

const submitBid = (itemId: string, bid: string, userId: string) => {
    try {
        return oioiApiInstance
          .post(`/api/bids/${itemId}`, {userId, bid});
    }
    catch (e) {
        return e;
    }
};


export default {
    submitBid,
};
