import axios from 'axios';
import TWITCH from '@/constants/constant-twitch';

///////////////////////////////////////////////////////////////////////////
// twitch instance
// Sets up the base information needed for twitch api
///////////////////////////////////////////////////////////////////////////

const twitchInstance = axios.create({
    baseURL: TWITCH.BASEURL,
    timeout: 3000,
    headers: {'Client-ID': TWITCH.CLIENT_KEY},
});

///////////////////////////////////////////////////////////////////////////
// getGamesFromTwitch
///////////////////////////////////////////////////////////////////////////

const getGamesFromTwitch = (userInput: string) => {
  try {
    return twitchInstance
      .get(TWITCH.SEARCHGAMES + encodeURI(userInput));
  }
  catch (e) {
    return e;
  }
};

///////////////////////////////////////////////////////////////////////////
// getGameById
// When a user picks a game we store the id in the store
///////////////////////////////////////////////////////////////////////////

const getGameById = (id: any) => {
  try {
    return twitchInstance
      .get(TWITCH.GETGAME + encodeURI(id));
  }
  catch (e) {
    return e;
  }
};

///////////////////////////////////////////////////////////////////////////
// getStreamsById
///////////////////////////////////////////////////////////////////////////

const getStreamsById = (id: any) => {
  try {
    return twitchInstance
      .get(TWITCH.GETGAME + encodeURI(id));
  }
  catch (e) {
    return e;
  }
};

export default {
  getGamesFromTwitch,
  getGameById,
  getStreamsById
};
