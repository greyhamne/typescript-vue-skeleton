import { oioiApiInstance } from '@/services/service-instances.api'

///////////////////////////////////////////////////////////////////////////
// Create draft - assigns id and creates directories etc on our server
///////////////////////////////////////////////////////////////////////////

const createDraft = (userId: string) => {
    try {
      return oioiApiInstance
        .post(`/api/listing/draft/${userId}`);
    }
    catch (e) {
      return e;
    }
}

///////////////////////////////////////////////////////////////////////////
// Create my listing
///////////////////////////////////////////////////////////////////////////

const createListing = (formData: any) => {
    try {
      return oioiApiInstance
        .post('/api/listing', formData, 
            {
                headers: { 'Content-Type': undefined},
            }
        );
    }
    catch (e) {
      return e;
    }
}

///////////////////////////////////////////////////////////////////////////
// Get my listing
///////////////////////////////////////////////////////////////////////////

const getMyListing = (id: number) => {
    try {
        return oioiApiInstance
          .get('/api/listing/' + id);
    }
    catch (e) {
        return e;
    }
};

///////////////////////////////////////////////////////////////////////////
// Delete my listing
///////////////////////////////////////////////////////////////////////////

const deleteMyListing = (id: number) => {
    try {
        return oioiApiInstance
          .delete('/api/listing/' + id);
    }
    catch (e) {
        return e;
    }
};

///////////////////////////////////////////////////////////////////////////
// Update my listing
///////////////////////////////////////////////////////////////////////////

const updateMyListing = (listing: any, listingId: string) => {

    try {
        return oioiApiInstance
        .patch(`/api/listing/${listingId}`, listing.formData, 
            {
                headers: { 'Content-Type': undefined},
            }
        );
    }
    catch (e) {
        return e;
    }
};

///////////////////////////////////////////////////////////////////////////
// Finish my listing
///////////////////////////////////////////////////////////////////////////

const finishListing = (listing: any, listingId: string) => {
    try {
        return oioiApiInstance
        .patch(`/api/listing/${listingId}`, listing);
    }
    catch (e) {
        return e;
    }
};

export default {
    createDraft,
    createListing,
    getMyListing,
    deleteMyListing,
    updateMyListing,
    finishListing
};
