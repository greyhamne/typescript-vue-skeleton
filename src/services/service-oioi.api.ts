import axios from 'axios';
import {oioiApiInstance} from '@/services/service-instances.api'

const oioiInstance = axios.create({
    baseURL: 'http://localhost:3001',
    timeout: 3000,
});

///////////////////////////////////////////////////////////////////////////
// POST Register a new user
///////////////////////////////////////////////////////////////////////////

const registerNewUser = (email: string, password: number | string, username: string | number) => {
    try {
        return oioiApiInstance
          .post('/api/users/register', {
              email,
              password,
              username
        });
    }
    catch (e) {
        return e;
    }
};

///////////////////////////////////////////////////////////////////////////
// POST Login a user
///////////////////////////////////////////////////////////////////////////

const loginUser = (email: string, password: string | number) => {
    try {
        return oioiApiInstance
        .post('/api/users/login', {
            email,
            password,
        });
    }
    catch (e) {
        return e;
    }
}

///////////////////////////////////////////////////////////////////////////
// POST check if username exists
///////////////////////////////////////////////////////////////////////////

const checkUsername = (username: string) => {
    try {
      return oioiApiInstance
        .post('/api/users/' + username);
    }
    catch (e) {
      return e;
    }
}

///////////////////////////////////////////////////////////////////////////
// POST check if email exists
///////////////////////////////////////////////////////////////////////////

const checkEmail = (email: string) => {
    try {
      return oioiApiInstance
        .post('/api/users/email/' + email);
    }
    catch (e) {
      return e;
    }
}

///////////////////////////////////////////////////////////////////////////
// POST add cover photo
///////////////////////////////////////////////////////////////////////////

const addCoverPhoto = (image: any, userId: string | number) => {
    try {
      return oioiApiInstance
        .patch('api/users/cover/' + userId , image,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
    );
    }
    catch (e) {
        return e;
    }
}

///////////////////////////////////////////////////////////////////////////
// POST add cover photo
///////////////////////////////////////////////////////////////////////////

const addProfilePhoto = (image: any, userId: string | number) => {
    try {
      return oioiApiInstance
        .patch('api/users/profile/' + userId , image,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
    );
    }
    catch (e) {
        return e;
    }
}

///////////////////////////////////////////////////////////////////////////
// PATCH to update details
///////////////////////////////////////////////////////////////////////////

const updateMyDetails = (userId: any, payload: {}) => {
    try {
        return oioiApiInstance
            .patch('api/users/' + userId , payload);
    }
    catch (e) {
        return e;
    }
}

  
// Move this


  const getListings = () => {
    try {
      return oioiApiInstance
        .get('/api/listings/');
    }
    catch (e) {
      return e;
    }
  }


export default {
    registerNewUser,
    loginUser,
    checkUsername,
    checkEmail,
    addCoverPhoto,
    addProfilePhoto,
    updateMyDetails,
    getListings
};
