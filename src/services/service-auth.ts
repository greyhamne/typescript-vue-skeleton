import { oioiApiInstance } from '@/services/service-instances.api'

///////////////////////////////////////////////////////////////////////////
// GET my listings
///////////////////////////////////////////////////////////////////////////


const inspectUserToken = (id: any) => {
    try {
        return oioiApiInstance
          .get('/api/listings/' + id);
    }
    catch (e) {
        return e;
    }
};

export default {
    inspectUserToken
};
