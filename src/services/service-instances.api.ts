import store from '@/store'
import axios from 'axios';

///////////////////////////////////////////////////////////////////////////
// oioi api instance
///////////////////////////////////////////////////////////////////////////
export const oioiApiInstance = axios.create({
    baseURL: 'http://localhost:3001',
    timeout: 3000,
});

oioiApiInstance.interceptors.request.use(function(config) {
    const jwt = store.getters['users/usersData/user'].token
  
    if ( jwt != null ) {
      config.headers.Authorization = `Bearer ${jwt}`;
    }

      return config;
  }, function(err) {
      return Promise.reject(err);
  });