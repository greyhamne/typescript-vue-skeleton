// TODO refresh user data on page load incase information like cover photo changes
// TODO if user clicks profile from the bottom nav we need to show the bottom toolbar 
// TODO work out v-model with computed
// TODO removing show states for full screen ui
// TODO add component names to constants

import Vue from 'vue';
import Router from 'vue-router';

import {ROUTE} from '@/constants/constant-route';

import Store from '@/store';

import Home from '@/pages/home/ViewHome.vue';
import Game from '@/pages/game/ViewGame.vue';
import NonAuth from '@/pages/nonAuth/ViewIndex.vue'
import Login from '@/pages/nonAuth/ViewLogin.vue';
import Register from '@/pages/nonAuth/ViewRegister.vue';
import PasswordReset from '@/pages/nonAuth/ViewPasswordReset.vue';
import Search from '@/pages/search/ViewSearch.vue';
import SearchResults from '@/pages/search/ViewSearchResults.vue';
import Marketplace from '@/pages/marketplace/ViewMarketplace.vue';
import Inn from '@/pages/inn/ViewInn.vue';
import Listing from '@/pages/inn/listings/_ViewListing.vue'
import EditListing from '@/pages/inn/listings/_ViewEditListing.vue'
import ViewProfile from '@/pages/profile/ViewMyProfile.vue'
import EditProfile from '@/pages/profile/ViewEditMyProfile.vue'
import CreateListing from '@/pages/marketplace/ViewCreateListing.vue'
import ViewListing from '@/pages/marketplace/ViewListing.vue'

Vue.use(Router);

const router =  new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        //////////////////////////////////
        // Redirects
        //////////////////////////////////
        { path: ROUTE.NON_AUTH.INDEX, redirect: ROUTE.NON_AUTH.LOGIN },
        { path: ROUTE.AUTH.HOME, redirect: ROUTE.AUTH.SEARCH.INDEX },
        //////////////////////////////////
        // Non Auth
        //////////////////////////////////
        {
            path:ROUTE.NON_AUTH.INDEX,
            name: 'nonAuth',
            component: NonAuth,
            //////////////////////////////////
            // Non Auth child routes
            //////////////////////////////////
            children: [
              {
                path: ROUTE.NON_AUTH.LOGIN,
                name: 'login',
                component: Login,
              },
              {
                path: ROUTE.NON_AUTH.REGISTER,
                name: 'Register',
                component: Register,
              },
              {
                path: ROUTE.NON_AUTH.PASSWORD_RESET,
                name: 'PasswordReset',
                component: PasswordReset,
              },
            ]
        },
        //////////////////////////////////
        // Profile Builder
        //////////////////////////////////
        {
          path: ROUTE.AUTH.PROFILE_BUILDER.INDEX,
          name: 'myProfile',
          component: ViewProfile,
        
        },
        //////////////////////////////////
        // Auth - with master template
        //////////////////////////////////
        {
          path: ROUTE.AUTH.HOME,
          name: 'home',
          component: Home,
          //////////////////////////////////
          // Validate user auth
          //////////////////////////////////
          beforeEnter: (to, from, next) => {
              const tokenStatus = Store.dispatch('auth/checkAuth', {}, { root: true })

              !tokenStatus ? next(ROUTE.AUTH.HOME) : next()
          },
          children: [

            //////////////////////////////////
            // Auth Routes
            //////////////////////////////////
            {
              path: ROUTE.AUTH.SEARCH.INDEX,
              name: 'search',
              component: Search,
            },
            {
              path: ROUTE.AUTH.SEARCH.RESULTS,
              name: 'searchResults',
              component: SearchResults,
            },

            //////////////////////////////////
            // game
            //////////////////////////////////

            {
              path: ROUTE.AUTH.GAME.INDEX,
              name: 'game',
              component: Game,
            },

            //////////////////////////////////
            // Marketplace
            //////////////////////////////////

            {
              path: ROUTE.AUTH.MARKETPLACE.MARKETHOME,
              name: 'market',
              component: Marketplace,
            },

            {
              path: ROUTE.AUTH.MARKETPLACE.CREATELISTING,
              name: 'create listing',
              component: CreateListing,
            },

            {
              path: ROUTE.AUTH.MARKETPLACE.VIEWLISTING,
              name: 'view listing',
              component: ViewListing,
            },

            //////////////////////////////////
            // Inn
            //////////////////////////////////

            {
              path: ROUTE.AUTH.INN.INDEX,
              name: 'inn',
              component: Inn,
            },
            {
              path: ROUTE.AUTH.INN.MYLISTING,
              name: 'myListing',
              component: Listing,
            },
            {
              path: ROUTE.AUTH.INN.MYLISTINGEDIT,
              name: 'myListingEdit',
              component: EditListing,
            },

            //////////////////////////////////
            // Profile
            //////////////////////////////////

            {
              path: ROUTE.AUTH.PROFILE.EDIT,
              name: 'editMyProfile',
              component: EditProfile,
            },
            
          ]
        },
        
    ],
});

router.beforeEach(async (to, from, next) => {
    const tokenStatus = await Store.dispatch('auth/checkAuth', {}, { root: true })

    console.log(tokenStatus)

    if(tokenStatus) {
      console.log("Token exists")
      if(to.name === 'login' || to.name === 'Register' || to.name === 'PasswordReset') {
          next(ROUTE.AUTH.HOME)
      } else {
          next()
      } 
        
    } else {
        next()
    }
})


export default router
