import { searchState } from '@/store/modules/search/search.module'
import { twitchState } from '@/store/modules/search/twitch/twitch.model';

export interface RootState {
    search: any;
    twitch: twitchState;
}

