import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist'

import auth from '@/store/modules/auth/store-auth'
import ui from '@/store/modules/ui/store-ui'
import search from '@/store/modules/search/store-search.module';
import game from '@/store/modules/game/store-game.module';
import users from '@/store/modules/users/store-users.module';
import marketplace from '@/store/modules/marketplace/store-marketplace';
import listing from '@/store/modules/listing/store-listing';
import listings from '@/store/modules/listings/store-listings';

Vue.use(Vuex);


const vuexLocal = new VuexPersistence({
    strictMode: false,
    key: 'oioi',
    storage: window.sessionStorage,
    
    reducer: (state: any) => ({
        users: {
            usersData: {
                token: state.users.usersData.token,
                email: state.users.usersData.email,
                id: state.users.usersData.id,
                cover: state.users.usersData.cover,
                profile: state.users.usersData.profile,
                firstname: state.users.usersData.firstname,
                secondname: state.users.usersData.secondname,
            }
        },
    }),
})

const Store =  new Vuex.Store({
    modules: {
        auth,
        ui,
        search,
        game,
        users,
        marketplace,
        listing,
        listings
    },
    plugins: [vuexLocal.plugin],
});

export default Store;
