import twitch from '@/store/modules/search/twitch/store-twitch.module';

const modules = {
    twitch,
};

const search =  {
    namespaced: true,
    modules,
};

export default search;
