import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import * as S from '@/store/modules/search/twitch/twitch.model';
import twitchApi from '@/services/service-twitch.api';

const state: S.TwitchState = {
    searchResult: [],
    recentSearches: [],
    loading: false,
};

const mutations: MutationTree<any> = {
    storeSearchResults(state: S.TwitchState, results: any) {

        // Empty array first
        state.searchResult = [];

        results.forEach((element: any) => {
            state.searchResult.push({
                id: element._id,
                name: element.name,
                cover: element.box.large,
                liked: false,
            });
        });
    },
    triggerLoadingState(state: S.TwitchState) {
        state.loading = !state.loading;
    },
    resetSearchResults(state: S.TwitchState) {
        state.searchResult = []
    }
};

const actions: ActionTree<S.TwitchState, any> = {
    getGamesFromTwitch({commit}, userInput: string) {
        commit('triggerLoadingState');

        twitchApi.getGamesFromTwitch(userInput)
            .then((res: any) => {
                commit('storeSearchResults', res.data.games);
            })
            .then(() =>
                commit('triggerLoadingState'));
    },
};

const getters: GetterTree<S.TwitchState, any> = {
    searchResult: (state: S.TwitchState) => state.searchResult,
};

export const twitch: Module<S.TwitchState, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default twitch;



