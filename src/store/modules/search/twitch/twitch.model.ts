export interface TwitchState {
  searchResult: SearchResult[];
  recentSearches: SearchResult[];
  loading: boolean;
}

export interface SearchResult {
  id: number;
  name: string;
  cover: string;
  liked: boolean;
}

