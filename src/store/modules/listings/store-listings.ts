import listingsApi from '@/store/modules/listings/api/store-listings-api';

const modules = {
    listingsApi,
};

const listings =  {
    namespaced: true,
    modules,
};

export default listings;
