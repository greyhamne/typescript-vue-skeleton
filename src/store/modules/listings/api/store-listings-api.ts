import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';

import listings from '@/services/service-listings.api';

interface state {
    loading: boolean;
    listingLoaded: boolean;
    myListings: myListings[];
    myListing: myListing;
    deletedListing: boolean;
    deletingListing: boolean;
    listingUpdated: boolean
}

interface myListings {
    id: number | string;
    title: string;
}

interface myListing {
    id: number | string;
    title: string;
}

const state: any = {
    loading: false,
    myListings: [],
    listingLoaded: false,
    myListing: [],
    deletedListing: false,
    deletingListing: false,
    listingUpdated: false,
};

const mutations: MutationTree<state> = {
    loading(state: state) {
        state.loading = !state.loading;
    },
    listingLoaded(state: state) {
        state.loading = false;
        state.listingLoaded = true
    },
    storeMyListings(state: state, myListings) {
        state.myListings = myListings
    },
    deletingListing(state: state) {
        state.deletingListing = !state.deletingListing
    },
    listingDeleted(state: state) {
        state.deletedListing = !state.deletedListing
    },
    listingUpdated(state: state) {
        state.listingUpdated = !state.listingUpdated
    },
    resetListing(state: state) {
        state.deletedListing = false
        state.deletingListing = false
        state.listingUpdated = false
    }
};

const actions: ActionTree<state, any> = {
    getMyListings({commit, rootGetters}) {
        commit('loading');

        const userId = rootGetters['users/usersData/user'].id

        listings.getMyListings(userId)
            .then((res: any) => {
                console.log(res.data)
                commit('storeMyListings', res.data)
            })
            .then(() =>
                commit('loading'))
            .catch((err: any) => {
                console.log(err)
            })
    },
};

const getters: GetterTree<state, any> = {
    myListings:(state: state) => state.myListings,
    myListing:(state: state) => state.myListing,
    listingLoaded:(state: state) => state.listingLoaded,
    deletedListing:(state: state) => state.deletedListing,
    deletingListing:(state: state) => state.deletingListing,
    updatedListing:(state: state) => state.listingUpdated,
};

export const listingsApi: Module<state, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default listingsApi;



