import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import serviceListing from '@/services/service-listing';
import createListing from '@/store/modules/listing/store-listing-create'

interface state {
    loading: boolean
    listingCreated: boolean
    listingLoaded: boolean
    myListing: myListing
    listingUpdated: boolean
    deletingListing: boolean
    deletedListing: boolean
}

interface myListing {
    id: string;
    title: string;
}

const modules = {
    createListing,
};
const state: any = {
    loading: false,
    listingCreated: false,
    listingLoaded: false,
    listingUpdated: false,
    deletingListing: false,
    deletedListing: false,
    myListing: [],
};

const mutations: MutationTree<state> = {
    processingListing(state: state) {
        state.loading = !state.loading;
    },
    listingCreated(state: state) {
        state.listingCreated = true;
    },
    loading(state: state) {
        state.loading = !state.loading;
    },
    storeMyListing(state: state, myListing) {
        state.myListing = myListing
    },
    listingUpdated(state: state) {
        state.listingUpdated = !state.listingUpdated
    },
    listingLoaded(state: state) {
        state.loading = false;
        state.listingLoaded = true
    },
    resetListing(state: state) {
        state.deletedListing = false
        state.deletingListing = false
        state.listingUpdated = false
    },
    deletingListing(state: state) {
        state.deletingListing = !state.deletingListing
    },
    listingDeleted(state: state) {
        state.deletedListing = !state.deletedListing
    },
};

const actions: ActionTree<state, any> = {
    createListing({commit, rootGetters}, listing) {

        // add auth'd users ID
        const userId = rootGetters['users/usersData/user'].id

        listing['userId'] = userId

        commit('processingListing');

        serviceListing.createListing(listing)
            .then((res: any) => {
                commit('listingCreated')
                commit("ui/showOverlay", null, { root: true })
            })
            .then(() =>
                commit('processingListing'))
            .catch((err: any) => {
                console.log(err)
        })
    },

    async getMyListing({commit}, id) {
        await commit('loading');

        serviceListing.getMyListing(id)
        .then((res: any) => {
            commit('storeMyListing', res.data)
        })
        .then(() =>
            commit('listingLoaded'))
        .catch((err: any) => {
            console.log(err)
        })
    },

    async deleteMyListing({commit}, id) {
        commit('deletingListing');

        serviceListing.deleteMyListing(id)
        .then((res: any) => {
            commit('listingDeleted', res.data)
            commit("ui/showOverlay", null, { root: true })
        })
        .then(() =>
            commit('deletingListing'))
        .catch((err: any) => {
            console.log(err)
        })
    },

    updateListing({commit, rootGetters}, listing,) {

        const userId = rootGetters['users/usersData/id']

        listing['userId'] = userId

        serviceListing.updateMyListing(listing, )
        .then((res: any) => {
            commit('listingCreated')
        })
        .then(() =>
            commit('listingUpdated'))
        .catch((err: any) => {
            console.log(err)
        })
    },

    resetListingStatus({commit}) {

        // reset ui
        commit('resetListing') 
    }
};

const getters: GetterTree<state, any> = {
    listingCreated: (state: state) => state.listingCreated,
    listingLoaded:(state: state) => state.listingLoaded,
    deletedListing:(state: state) => state.deletedListing,
    deletingListing:(state: state) => state.deletingListing,
    updatedListing:(state: state) => state.listingUpdated,
    myListing:(state: state) => state.myListing,
};

export const listingsApi: Module<state, any> = {
    modules,
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default listingsApi;



