import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import serviceListing from '@/services/service-listing';

interface state {
    listingId: any // why is this failing ??
    title: string,
    descriptionType: string
    description: string,
    listingType: string,
    deliveryType: string,

    images: string[],
    delivery: string,
    method: string,
    cost: string,
    postage: string,
    latitude: string,
    longitude: string

    auctionStartPrice: string
    auctionStartDate: string 
    auctionStartTime: string
}

const state: state = {
    listingId: '',
    title:'',
    descriptionType: '',
    description:'',
    listingType:'',
    deliveryType: '',

    images: [],
    delivery: '',
    method: '',
    cost: '',
    postage: '',
    latitude: '',
    longitude: '',

    auctionStartPrice: '',
    auctionStartDate: '',
    auctionStartTime: ''
};

const mutations: MutationTree<state> = {
    saveLocation(state: state, {latitude, longitude}) {
        state.longitude = longitude
        state.latitude = latitude
    },
};

const actions: ActionTree<state, any> = {

    // TODO probs should be in userData 
    storeLocation({commit}, pos) {
        const {latitude, longitude} = pos.coords

        commit('saveLocation', {latitude, longitude})

    }, 



    createListing({commit, state, rootGetters}, formData) {
        const {id} = rootGetters['users/usersData/user']

        formData.append('userId', id)
        formData.append('longitude', state.longitude)
        formData.append('latitude', state.latitude)

        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]); 
        }
   
        serviceListing.createListing(formData)
        .then((res: any) => {
            commit("ui/showOverlay", null, { root: true })
        })
        .catch((err: any) => {
            console.log(err)
        })
    }
};

const getters: GetterTree<state, any> = {
    id: (state: state) => state.listingId,
    images: (state: state) => state.images,

    previewListing: (state: state) => {
        return {
            title: state.title,
            description: state.description,
            cost: state.cost,
            postage: state.postage,
            method: state.listingType,
            longitude: state.longitude,
            latitude: state.latitude,
            auctionStartPrice: state.auctionStartPrice,
            auctionStartDate: state.auctionStartDate,
            auctionStartTime: state.auctionStartTime
        }
    },

    descriptionType: (state: state) => state.descriptionType,
    listingType: (state: state) => state.listingType,
    deliveryType: (state: state) => state.deliveryType
};

export const createListing: Module<state, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default createListing;



