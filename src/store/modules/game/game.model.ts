export interface GameState {
    basic: {};
    recentlyViewed: Basic[];
    streams: any;
}

export interface Basic {
    id: number | string;
    title: string;
}