import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import * as S from '@/store/modules/game/game.model';

import {uniqBy} from 'lodash';

const state: S.GameState = {
    basic: {},
    recentlyViewed: [],
    streams: {},
};

const mutations: MutationTree<any> = {
    saveSearchAndGo(state: S.GameState, game) {
        state.recentlyViewed.push(game)
        state.basic = game;
    }
};

const actions: ActionTree<S.GameState, any> = {
};

const getters: GetterTree<S.GameState, any> = {
    recentlyViewed: (state: S.GameState) => uniqBy(state.recentlyViewed, 'id'),
};

export const game: Module<S.GameState, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default game;



