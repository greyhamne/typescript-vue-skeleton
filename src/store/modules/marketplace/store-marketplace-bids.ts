import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import ServiceMarketplaceBids from '@/services/service-marketplace-bids';

interface state {
    bid: number
    responseToBid: string
    showResponseToBid: boolean
}

const state: any = {
    bid: 0,
    responseToBid: '',
    showResponseToBid: false
};


const mutations: MutationTree<state> = {
 
    showResponseToBid(state: state, msg: state["responseToBid"]) {
        state.responseToBid = msg
        state.showResponseToBid = true
    }
};

const actions: ActionTree<state, any> = {

    placeBid ({commit, state, rootGetters}, {itemId, bid}) {
        
        const {id} = rootGetters['users/usersData/user']

        ServiceMarketplaceBids.submitBid(itemId, bid, id)
        .then((res: any) => {
            commit('showResponseToBid', res.data.msg)
        })
        .catch((err: any) => {
            console.log(err)
        });
    },

};

const getters: GetterTree<state, any> = {
    response: (state: state) => {
        return {
            showMessage: state.showResponseToBid,
            message: state.responseToBid
        }
    }
};

export const marketPlaceBids: Module<state, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default marketPlaceBids;



