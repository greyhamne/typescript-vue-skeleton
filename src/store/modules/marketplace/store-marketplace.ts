import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import marketplaceBids from '@/store/modules/marketplace/store-marketplace-bids'
import ServiceMarketplace from '@/services/service-marketplace';

interface state {
    listings: any[] // TO DO type this once we know what were getting
    listing: {}
}

const state: any = {
    listings: [],
    listing: {}
};


const modules = {
    marketplaceBids
}

const mutations: MutationTree<state> = {
    saveListings(state: state, listings: any[]) {
        state.listings = listings
    },
    saveListing(state: state, listing: {}) {
        state.listing = listing
    }
};

const actions: ActionTree<state, any> = {
    
    getAllListings ({commit,state, rootGetters}) {

        const {id} = rootGetters['users/usersData/user']

        console.log(id)

        ServiceMarketplace.getListings(id)
            .then((res: any) => {
                commit('saveListings', res.data)
            })
            .catch((err: any) => {
                console.log(err)
            })
    },

    getListing ({commit}, itemId: number) {
        ServiceMarketplace.getListing(itemId)
            .then((res: any) => {
                console.log(res.data)
                commit('saveListing', res.data)
            })
            .catch((err: any) => {
                console.log(err)
            })
    }
};

const getters: GetterTree<state, any> = {
    listings: (state: state) => state.listings,
    listing: (state: state) => state.listing
};

export const usersApi: Module<state, any> = {
    modules,
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default usersApi;



