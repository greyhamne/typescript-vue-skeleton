import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';

import router from '@/router'

interface state {
    isOverlayVisible: boolean
}

const state: state = {
    isOverlayVisible: false
};

const mutations: MutationTree<state> = {
    showOverlay (state: state) {
        state.isOverlayVisible = true
    },
    hideOverlay (state: state) {
        state.isOverlayVisible = false
    }
    
};

const actions: ActionTree<state, any> = {
    hideOverlay({commit}) {
        commit('hideOverlay')
    }
};

const getters: GetterTree<state, any> = {
    isOverlayVisible: (state: state) => state.isOverlayVisible
};

export const ui: Module<state, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default ui;



