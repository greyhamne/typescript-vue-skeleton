import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import jwtDecode from 'jwt-decode'
import moment from 'moment'

interface state {

}

const state = {

};

const mutations: MutationTree<any> = {
   
};

const actions: ActionTree<state, any> = {
    checkAuth({commit, rootGetters}, listing) {

        // add auth'd users ID
        const {email} = rootGetters['users/usersData/user']
        const {token} = rootGetters['users/usersData/user']

        if(!email && !token) {
            console.log('Token and/or email not found logging out')
            return false
        } 

        if (token) {
 
            const expiryTimeInSeconds = jwtDecode(token).exp
            const timeNowInSeconds = moment().unix()
            const secondsUntilExpiry = expiryTimeInSeconds - timeNowInSeconds
            const tokenHasExpired = secondsUntilExpiry <= 0

            if(tokenHasExpired) {
                console.log('token has expired')
                return false
            } else {
                return true
            }
        }

        console.log('check auth hit')

        // user is logged in so return some kind of success
    },
};

const getters: GetterTree<state, any> = {

};

export const sell: Module<state, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default sell;



