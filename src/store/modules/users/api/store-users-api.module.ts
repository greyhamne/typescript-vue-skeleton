import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';

import oioiApi from '@/services/service-oioi.api';

import router from '@/router'

interface state {
    loading: boolean
    userRegistered: boolean
    isUsernameTaken: boolean
    isEmailTaken: boolean
    loginFailed: boolean
}

const state: any = {
    loading: false,
    userRegistered: false,
    isUsernameTaken: false,
    isEmailTaken: false,
    loginFailed: false
};

const mutations: MutationTree<state> = {
    triggerLoadingState(state: state) {
        state.loading = !state.loading;
    },
    userHasRegistred(state: state) {
        state.userRegistered = true
    },
    isUsernameTaken(state: state, isUsernameTaken) {
        state.isUsernameTaken = isUsernameTaken
    },
    isEmailTaken(state: state, isEmailTaken) {
        state.isEmailTaken = isEmailTaken
    },
    loginFailed(state: state, loginFailed) {
        state.loginFailed = true
    }
    
};

// TODO Add proper loading states for UI on async tasks

const actions: ActionTree<state, any> = {
    newUser({commit}, {email, password, username}) {
        console.log(username)
        commit('triggerLoadingState');

        oioiApi.registerNewUser(email, password, username)
            .then((res: any) => {
                commit("userHasRegistred")
                commit("ui/showOverlay", null, { root: true })
            })
            .then(() =>
                commit('triggerLoadingState'))
            .catch((err: any) => {
                console.log(err)
            })
    },
    loginUser({commit, dispatch}, {email, password}) {
        window.localStorage.removeItem('oioi')
        commit('triggerLoadingState');

        oioiApi.loginUser(email, password)
            .then((res: any) => {
                const userData = res.data
                dispatch('users/usersData/storeCurrentData', {userData}, { root: true })
                router.push('/home')
            })
            .then(() =>
                commit('triggerLoadingState'))
            .catch((err: any) => {
                //err.response.status === 401 ? commit('loginFailed') : null
                console.log(err)
            })
    },
    checkIfUsernameExists({commit}, username: any) {
        oioiApi.checkUsername(username)
            .then((res: any) => {
                commit('isUsernameTaken', res.data.isUsernameTaken)
            })
            .then(() =>
                commit('triggerLoadingState'))
            .catch((err: any) => {
                console.log(err)
            })
    },
    checkIfEmailExists({commit}, email: any) {
        oioiApi.checkEmail(email)
            .then((res: any) => {
                commit('isEmailTaken', res.data.isEmailTaken)
            })
            .then(() =>
                commit('triggerLoadingState'))
            .catch((err: any) => {
                console.log(err)
            })
    },
    addCoverPhoto({commit, rootGetters}, file) {

        const user = rootGetters['users/usersData/user']

        oioiApi.addCoverPhoto(file, user.id)
            .then((res: any) => {
                console.log(res)
            })
            .then(() =>
                commit('triggerLoadingState'))
            .catch((err: any) => {
                console.log(err)
            })
    },
    addProfilePhoto({commit, rootGetters}, file) {

        const user = rootGetters['users/usersData/user']

        oioiApi.addProfilePhoto(file, user.id)
            .then((res: any) => {
                console.log(res)
            })
            .then(() =>
                commit('triggerLoadingState'))
            .catch((err: any) => {
                console.log(err)
            })
    },
    updateMyInformation({commit, rootGetters}, payload) {
        const user = rootGetters['users/usersData/user']

        oioiApi.updateMyDetails(user.id, payload)
            .then((res: any) => {
                console.log(res)
            })
            .then(() =>
                commit('triggerLoadingState'))
            .catch((err: any) => {
                console.log(err)
            })
    }
};

const getters: GetterTree<state, any> = {
    userHasRegistered:(state) => state.userRegistered,
    isUsernameTaken:(state) => state.isUsernameTaken,
    isEmailTaken:(state) => state.isEmailTaken,
    loginFailed:(state) => state.loginFailed
};

export const usersApi: Module<state, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default usersApi;



