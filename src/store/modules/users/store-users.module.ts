import usersApi from '@/store/modules/users/api/store-users-api.module';
import usersData from '@/store/modules/users/data/store-users-data.module';

const modules = {
    usersApi,
    usersData
};

const users =  {
    namespaced: true,
    modules,
};

export default users;
