import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';

interface state {
    email: any;
    id: number | string;
    token: number | string;
    cover: string;
    profile: string;
    firstname: string 
    secondname: string
}

const state: state = {
    email: '',
    id: '',
    token: '',
    cover: '', 
    profile: '',
    firstname: '',
    secondname: '',
};

const mutations: MutationTree<state> = {
    storeCurrentData(state: state, userData) {
        state.email = userData.userData.email
        state.id = userData.userData.userId
        state.token = userData.userData.token
        state.cover = userData.userData.cover
        state.profile = userData.userData.profile
        state.firstname = userData.userData.firstname
        state.secondname = userData.userData.secondname
    }
};

const actions: ActionTree<state, any> = {
    storeCurrentData({commit}, userData) {
        commit('storeCurrentData', userData)
    },
    checkUserToken({ commit, state, getters, dispatch, rootGetters }) {

        const jwt = getters.token

        if(jwt === "") {
            return 'no token'
        } else {
            return jwt
        }
    }
};

const getters: GetterTree<state, any> = {
    user: (state: state) => {
        return {
            email: state.email,
            id: state.id,
            token: state.token,
            profile: state.profile,
            cover: state.cover,
            firstname: state.firstname,
            secondname: state.secondname,
        }
    },
};

export const usersData: Module<state, any> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

export default usersData;



