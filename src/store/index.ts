import { namespace } from 'vuex-class';

export default {
    Auth:                namespace('auth'),
    Ui:                  namespace('ui'),
    Search:              namespace('search'),
    SearchTwitch:        namespace('search/twitch'),
    Game:                namespace('game'),
    GameSell:            namespace('game/sell'),
    Users:               namespace('users'),
    UsersApi:            namespace('users/usersApi'),
    UsersData:           namespace('users/usersData'),
    Marketplace:         namespace('marketplace'),
    MarketplaceBids:     namespace('marketplace/marketplaceBids'),
    Listing:             namespace('listing'),
    ListingCreate:       namespace('listing/createListing'),
    Listings:            namespace('listings'),
    ListingsApi:         namespace('listings/listingsApi'),
};
