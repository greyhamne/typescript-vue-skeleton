import Vue from 'vue';

// Templates and Components for generic layotus

Vue.component('ui-header', () => import('@/components/Header/UiHeader.vue'));
Vue.component('ui-body', () => import('@/components/Layout/UiGeneralBodyLayout.vue'));
Vue.component('ui-non-auth', () => import('@/components/Layout/UiNonAuthLayout.vue'));
Vue.component('ui-auth', () => import('@/components/Layout/UiAuthLayout.vue'));
Vue.component('ui-bottom-nav', () => import('@/components/Layout/UiBottomNav.vue'));

// Resuable 
Vue.component('ui-twitch-list', () => import('@/components/Search/Twitch/UiTwitchResult.vue'));
Vue.component('ui-fullscreen', () => import('@/components/General/UiFullScreenMsg.vue'));
Vue.component('ui-back', () => import('@/components/General/UiBackButton.vue'));
Vue.component('ui-listing-form', () => import('@/components/General/UiListingForm.vue'));
Vue.component('ui-listing-card', () => import('@/components/General/UiListingCard.vue'));
Vue.component('ui-cover-pic', () => import('@/components/General/UiCoverPic.vue'));
Vue.component('ui-profile-pic', () => import('@/components/General/UiProfilePic.vue'));
Vue.component('ui-select-image', () => import('@/components/General/UiSelectImage.vue'));


