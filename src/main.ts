import Vue from 'vue';
import App from './App.vue';
import router from '@/router';
import store from '@/store';
import Vuetify from 'vuetify';
import Vuelidate from 'vuelidate';
import '@/components/components';
import 'vuetify/dist/vuetify.min.css';
import '@/assets/style/main.scss';
import VueGeolocation from 'vue-browser-geolocation';
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.config.productionTip = false;

Vue.use(Vuelidate)
Vue.use(Vuetify);
Vue.use(VueGeolocation);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBj5MXOP8pyuFCLb_nnnDOI-6spPLQyrLA',
    libraries: 'places', // This is required if you use the Autocomplete plugin
  },
})

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
