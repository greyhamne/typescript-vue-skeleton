export enum LISTING_METHOD {
    BIN = 'BuyItNow',
    AUCTION = '99p' 
}

export default LISTING_METHOD;
  