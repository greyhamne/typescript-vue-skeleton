export enum TWITCH {
  BASEURL = 'https://api.twitch.tv',
  CLIENT_KEY = 'zm9y00070a3i78xtdx4hy8gon8ltz7',
  SEARCHGAMES = '/kraken/search/games?type=suggest&query=',
  GETGAME = '/helix/games?id=',
  GETSTREAMS = '/kraken/streams/?game_id=',
}

export default TWITCH;
