const ROUTE = {
    NON_AUTH: {
        INDEX:'/',
        LOGIN: '/login',
        REGISTER: '/register',
        PASSWORD_RESET: '/password-reset',
    },
    AUTH: {
        HOME: '/home',

        PROFILE_BUILDER: {
            INDEX: '/profile'
        },

        SEARCH: {
           INDEX: '/search',
           RESULTS: '/search-results',
        },

        GAME: {
            INDEX: '/game',
        },

        MARKETPLACE: {
            MARKETHOME: '/marketplace',
            CREATELISTING: '/marketplace/create',
            VIEWLISTING: '/marketplace/view/:id'
        },

        INN:{
            INDEX: '/inn',
            MYLISTINGS:'/inn/mylistings',
            MYLISTING:'/inn/mylisting/:id',
            MYLISTINGEDIT:'/inn/mylisting/edit/:id',
        },

        PROFILE: {
            EDIT: '/MyProfile'
        }
    },
};

export {
    ROUTE,
};

